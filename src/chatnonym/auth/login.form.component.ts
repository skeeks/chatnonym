import { Component } from '@angular/core';
import { StaticLogger } from 'chatnonym/utils';

import { LoginModel } from './login.model';


@Component({
  selector: 'login-form',
  template: require('./login.form.view.html')
})
export class LoginFormComponent {
  login: LoginModel;
  isStoring: boolean = false;

  constructor() {
  }

  ngOnInit() {
    StaticLogger.info('Initialized LoginFormComponent');
    this.login = new LoginModel();
  }

  doStore() {
    this.isStoring = true;

    // this.isStoring = false;
  }
}
