import { Component } from '@angular/core';
import { StaticLogger } from 'chatnonym/utils';

import { RegisterModel } from './register.model';

@Component({
  selector: 'register-form',
  template: require('./register.form.view.html')
})
export class RegisterFormComponent {
  register: RegisterModel;

  constructor() {

  }

  ngOnInit() {
    StaticLogger.info('Initialized RegisterFormComponent');
    this.register = new RegisterModel();
  }

  doStore() {

  }

}
