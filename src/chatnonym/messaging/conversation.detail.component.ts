import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute, Params } from '@angular/router';

import { StaticLogger } from 'chatnonym/utils';

import { MessagingService } from './messaging.service';
import { Conversation } from './conversation.model';


@Component({
  selector: 'conversation-detail',
  template: require('./conversation.detail.view.html')
})
export class ConversationDetailComponent {
  private error: string;
  private conversation: Conversation;
  private newmessage: string;

  constructor(private msgService: MessagingService, private router: Router, private route: ActivatedRoute) {

  }

  ngOnInit() {
    StaticLogger.info('Initialized ConversationDetailComponent!');
    this.route.params.forEach((params: Params) => {
      let chatId = params['id'];
      this.load(chatId);
    });
  }

  protected load(id: string): void {
    let that = this;

    // load the chat here
    this.msgService.load(id).subscribe(
      (conversation: Conversation) => {
        that.conversation = conversation;
      },
      (error: any) => {
        that.error = String(error);
      }
    );
  }

  public doWrite() {
    // process send message here
  }
}
