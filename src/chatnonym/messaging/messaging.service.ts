import { Injectable } from '@angular/core';
import { User } from 'chatnonym/user';
import {Observable} from "rxjs/Rx";
import { Conversation } from './conversation.model';
import { Message } from './message.model';

@Injectable()
export class MessagingService {
  public loadAllMeta(): Observable<Conversation[]> {
    return Observable.of([
      new Conversation(new User('11', 'skeeks'), 35687345, 2, []),
      new Conversation(new User('351', 'lola'), 2412781, 1, []),
      new Conversation(new User('54', 'ifkurv'), 523525235, 0, []),
      new Conversation(new User('66', 'mangrod'), 773662, 0, []),
      new Conversation(new User('66', 'sixtisam'), 8959679856, 0, []),
    ]);
  }

  public load(partnerId: string): Observable<Conversation> {
    return Observable.of(new Conversation(
      new User('11', 'skeeks'),
      2562626,
      1,
      [
        new Message(true, 'Hey wie gohts dir so?', 2525325),
        new Message(false, 'Guet?', 56743455),
        new Message(false, 'und dir?', 56743455),
        new Message(true, 'Ja mir au', 4353472),
        new Message(false, 'Supi', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525),
        new Message(false, 'Spam', 65823525)
      ]
    ));
  }
}
