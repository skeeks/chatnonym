import { User } from 'chatnonym/user';
import { Message } from './message.model';

export class Conversation {

  constructor(public partner: User, public lastContact: number, public unreadMessages: number, public messages: Message[]) { }
}
