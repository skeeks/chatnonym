import { User } from 'chatnonym/user';

export class Message {

  constructor(public isWrittenByMyself: boolean, public text: string, public timestamp: number) { }
}
