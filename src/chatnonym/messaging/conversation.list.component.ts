import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute, Params } from '@angular/router';

import { StaticLogger } from 'chatnonym/utils';

import { MessagingService } from './messaging.service';
import { Conversation } from './conversation.model';


@Component({
  selector: 'conversation-list',
  template: require('./conversation.list.view.html')
})
export class ConversationListComponent {
  private conversations: Conversation[];
  private error: string;
  constructor(private msgService: MessagingService, private router: Router, private route: ActivatedRoute) {

  }

  ngOnInit() {
    StaticLogger.info('Initialized ConversationListComponent!');
    this.load();
  }

  protected load(): void {
    let that = this;

    // load all conversations
    this.msgService.loadAllMeta().subscribe(
      (conversations: Conversation[]) => {
        that.conversations = conversations;
      },
      (error: any) => {
        that.error = String(error);
      }
    );
  }
}
