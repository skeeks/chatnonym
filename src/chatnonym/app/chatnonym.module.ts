import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';


import { routing }        from './app.routing';

/*
* Platform and Environment
* our providers/directives/pipes
*/
import { PLATFORM_PROVIDERS } from 'platform/browser';
import { ENV_PROVIDERS } from 'platform/environment';

import { HttpModule } from '@angular/http';

import { AppComponent }  from './app.component';
import { UserService } from 'chatnonym/user';
import { LoginFormComponent, RegisterFormComponent, AuthService } from 'chatnonym/auth';
import { PostService, PostListComponent, PostDetailComponent, PostFormComponent, PostComponent } from 'chatnonym/post';
import { NavigationComponent } from 'chatnonym/navigation';
import { PreloaderComponent, ButtonPreloaderComponent } from 'chatnonym/preloader';
import { LocatorService, LocatorComponent } from 'chatnonym/locator';
import { MessagingService, ConversationListComponent, ConversationDetailComponent } from 'chatnonym/messaging';
import { TimeAgoPipe } from 'chatnonym/pipes';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    routing,
    HttpModule
  ],
  declarations: [
    AppComponent,
    NavigationComponent,
    LoginFormComponent,
    PreloaderComponent,
    ButtonPreloaderComponent,
    RegisterFormComponent,
    PostListComponent,
    PostComponent,
    PostDetailComponent,
    PostFormComponent,
    LocatorComponent,
    TimeAgoPipe,
    ConversationListComponent,
    ConversationDetailComponent
  ],
  bootstrap: [
    AppComponent
  ],
  providers: [
    ...PLATFORM_PROVIDERS,
    ...ENV_PROVIDERS,
    UserService,
    PostService,
    AuthService,
    LocatorService,
    MessagingService
  ]
})
export class ChatnonymModule { }
