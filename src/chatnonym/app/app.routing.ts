import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginFormComponent, RegisterFormComponent } from 'chatnonym/auth';
import { PostListComponent, PostDetailComponent, PostFormComponent } from 'chatnonym/post';
import { ConversationDetailComponent, ConversationListComponent } from 'chatnonym/messaging';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'posts',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginFormComponent
  },
  {
    path: 'register',
    component: RegisterFormComponent
  },
  {
    path: 'posts',
    component: PostListComponent
  },
  {
    path: 'post/detail/:id',
    component: PostDetailComponent
  },
  {
    path: 'post/create',
    component: PostFormComponent
  },
  {
    path: 'conversations',
    component: ConversationListComponent
  },
  {
    path: 'conversations/:id',
    component: ConversationDetailComponent
  }
];

export const routing = RouterModule.forRoot(appRoutes);
