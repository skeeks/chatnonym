/*
 * Angular 2 decorators and services
 */
import { Component } from '@angular/core';
import { StaticLogger } from 'chatnonym/utils';

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  template: require('./app.view.html')
})
export class AppComponent {

  constructor() {
  }

  ngOnInit() {
    StaticLogger.info('Initialized Chatnonym!');
  }
}
