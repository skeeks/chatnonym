import { User } from 'chatnonym/user';
import { Comment } from './comment.model';

export class Post {

  constructor(
    public id?: string, public text?: string,
    public location?: string,
    public timestamp?: number, public user?: User,
    public feedback?: number, public comments?: Comment[]) {
  }
}
