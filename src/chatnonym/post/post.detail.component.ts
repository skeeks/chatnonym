import { Component } from '@angular/core';
import { StaticLogger } from 'chatnonym/utils';
import { Router } from '@angular/router';
import { PostService } from './post.service';
import { Post } from './post.model';
import { ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'post-detail',
  template: require('./post.detail.view.html')
})
export class PostDetailComponent {
  private error: string;
  private post: Post;

  constructor(private postService: PostService, private router: Router, private route: ActivatedRoute) {

  }

  ngOnInit() {
    StaticLogger.info('Initialized PostDetailComponent!');
    this.route.params.forEach((params: Params) => {
      let postId = params['id'];
      this.load(postId);
    });
  }

  protected load(id: string): void {
    let that = this;

    that.postService.load(id).subscribe(
      (post: Post) => {
        that.post = post;
      },
      (error: any) => {
        that.error = String(error);
      }
    );
  }
}
