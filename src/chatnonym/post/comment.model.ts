import { User } from 'chatnonym/user';

export class Comment {

  constructor(public id?: string, public text?: string, public timestamp?: number, public user?: User) {
  }
}
