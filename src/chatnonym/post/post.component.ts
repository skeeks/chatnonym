import { Component, Input } from '@angular/core';
import { Post } from './post.model';

@Component({
  selector: 'post',
  template: require('./post.view.html')
})
export class PostComponent {
  @Input()
  post: Post;

  @Input()
  hideComments: boolean;
}
