import { Component } from '@angular/core';
import { StaticLogger } from 'chatnonym/utils';
import { Router } from '@angular/router';
import { PostService } from './post.service';
import { Post } from './post.model';
import { ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'post-form',
  template: require('./post.form.view.html')
})
export class PostFormComponent {
  private error: string;
  private post: Post;

  constructor(private postService: PostService, private router: Router, private route: ActivatedRoute) {

  }

  ngOnInit() {
    StaticLogger.info('Initialized PostFormComponent!');
    if (!this.post) {
      this.post = {};
    }
  }

  doStore() {
    let that = this;
    that.postService.store(that.post).subscribe(
      (result: boolean) => {
        that.router.navigate(['/posts']);
        that.post = null;
      },
      (error: any) => {
        that.error = String(error);
      }
    );
  }
}
