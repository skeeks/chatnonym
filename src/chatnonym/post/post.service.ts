import { Injectable } from '@angular/core';
import { Post } from './post.model';
import { User } from 'chatnonym/user';
import { Observable } from "rxjs/Rx";
import { Comment } from './comment.model';

@Injectable()
export class PostService {
  public loadAll(): Observable<Post[]> {
    return Observable.of([
      new Post('1', 'Dies ist ein Testpost', 'Villmergen, Schweiz', 1471421160027, new User('1', 'samuelx'), 0),
      new Post('2', 'Hihihi isch ja mega lustig do.', 'Hilfikon, Schweiz', 1471121160027, new User('2', 'claudiax'), -1),
      new Post('3', 'Lorem ipsums dolorus sit ametus.', 'Spreitenbach', 1471021160027, new User('3', 'janx'), 1),
      new Post('4', 'Lack du mir', 'Bern, Schweiz', 1371421160027, new User('3', 'blaubär'), -1),
      new Post('5', 'Hehe wer het hüt no nüt vor?', 'Baden, Schweiz', 1431421160027, new User('2', 'claudiax'), 0),
      new Post('6', 'Da lauft mer 1h im wald ume und findet doch kei pokemon -.-', 'Dättwil, Schweiz', 1471321160027, new User('5', 'ichbenschocool'), 0),
      new Post('7', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Waltenschwil, Schweiz', 1471420000027, new User('6', 'DickerWal'), 1)
    ]);
  }

  public load(id: string): Observable<Post> {
    return Observable.of(
      new Post('1',
        'Das isch de standard post. es wird immer de glade',
        'Villmergen, Schweiz',
        1471419420091,
        new User('2', 'claudiax'),
        1,
        [
          new Comment('22', 'da chunnt ja no zerscht de text', 1471420772550, new User('10', 'samuelk')),
          new Comment('12', 'Ey isch grad mega lustig, das sött en zemlich lange text werden. ' +
            'Eifach wills cool isch. Isch das ned geil? Oder wart....... Ich weiss bald nömm was schribe. :-) tschüüssli', 147141943391, new User('12', 'testuser')),
          new Comment('14', 'ich wott no viel meh comments ha', 471419453391, new User('10', 'samuelk')),
          new Comment('19', 'Isch richtig nice', 471419450191, new User('12', 'GrosserBär'))
        ]
      )
    );
  }

  public store(post: Post): Observable<boolean> {
    return Observable.of(true);
  }

  public delete(post: Post): Observable<boolean> {
    return Observable.of(true);
  }

  public report(post: Post): Observable<boolean> {
    return Observable.of(true);
  }
}
