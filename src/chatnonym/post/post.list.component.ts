import { Component } from '@angular/core';
import { StaticLogger } from 'chatnonym/utils';
import { Router } from '@angular/router';
import { PostService } from './post.service';
import { Post } from './post.model';


@Component({
  selector: 'post-list',
  template: require('./post.list.view.html')
})
export class PostListComponent {
  private error: string;
  private posts: Post[];

  constructor(private postService: PostService, private router: Router) {

  }

  ngOnInit() {
    StaticLogger.info('Initialized PostListComponent!');
    this.load();
  }

  protected load(): void {
    let that = this;

    that.postService.loadAll().subscribe(
      (posts: Post[]) => {
        that.posts = posts;
      },
      (error: any) => {
        that.error = String(error);
      }
    );
  }

  doCreatePost() {
    this.router.navigate(['/post/create']);
  }
}
