import { Component, Input } from '@angular/core';
import { StaticLogger } from 'chatnonym/utils';
import { Router } from '@angular/router';

import { ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'navigation',
  template: require('./navigation.view.html')
})
export class NavigationComponent {
  private navLinks: any[];

  @Input()
  onlyBack: boolean = false;

  constructor(private router: Router, private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.navLinks = [
      { path: '/login', title: 'Einloggen' },
      { path: '/register', title: 'Registrieren' },
      { path: '/posts', title: 'Posts' },
      { path: '/conversations', title: 'Chats' }
    ];
  }

  ngAfterViewInit() {
    eval('$(".side-nav-button").sideNav();'); // eval this code, because else, it is marked as an error
    eval('$("#side-nav").on("click", "a", function(){$(".side-nav-button").sideNav("hide");})'); // eval this code, because else, it is marked as an error
  }

}
