import { Component, Input  } from '@angular/core';

import { LocatorService } from './locator.service';


const CURRENT_LOCATION_KEY = "chatnonym.current.location";

@Component({
  selector: 'locator',
  template: require('./locator.view.html')
})
export class LocatorComponent {
  location: string;

  constructor(private locator: LocatorService) { }

  ngOnInit() {
    if (window.localStorage.getItem(CURRENT_LOCATION_KEY)) {
      this.location = window.localStorage.getItem(CURRENT_LOCATION_KEY);
    } else {
      this.doLocate();
    }
  }

  doLocate() {
    let that = this;
    this.locator.locate().subscribe(
      (location: string) => {
        if (location) {
          that.location = location;
          window.localStorage.setItem(CURRENT_LOCATION_KEY, location);
        }
      },
      (error: string) => {
        alert(error);
      });
  }
}
