import { Injectable } from '@angular/core';
import { Observable, Observer } from "rxjs/Rx";
import { Http, Response } from '@angular/http';

const GEOCODER_API_URL = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=%lat%,%lng%';

@Injectable()
export class LocatorService {

  constructor(private http: Http) { }


  public locate(): Observable<string> {
    let that = this;
    if (!navigator.geolocation) {
      return Observable.throw(new Error('Konnte das Gerät nicht orten!'))
    }
    return Observable.create(function(observer: Observer<string>) {
      var geolocationCallback = function(geoposition) {
        if (!geoposition) {
          observer.error('Konnte das Gerät nicht orten');
          observer.complete();
        }
        let url = GEOCODER_API_URL.replace('%lat%', geoposition.coords.latitude).replace('%lng%', geoposition.coords.longitude);
        that.http.get(url).subscribe(
          (response: Response) => {
            let mapsResp = response.json();

            let location = null;
            console.log(mapsResp);
            for (var n in mapsResp.results) {
              if (mapsResp.results[n] && mapsResp.results[n].types[0] == 'locality') {
                location = mapsResp.results[n].formatted_address;
                break;
              }
            }
            observer.next(location);
            observer.complete();
          },
          (response: Response) => {
            observer.error('Google Maps API konnte nicht erreicht werden');
            observer.complete();
          });
      };

      navigator.geolocation.getCurrentPosition(geolocationCallback);
    });
  }
}
