import { Injectable } from '@angular/core';

import { User } from './user.model';
import {Observable} from "rxjs/Rx";

@Injectable()
export class UserService {
  public loadAll(): Observable<User[]> {
    return Observable.of([
      new User('1', 'samuelx')
    ]);
  }

  public load(): Observable<User> {
    return Observable.of(new User('1', 'samuelx'));
  }

  public store(post: User): Observable<boolean> {
    return Observable.of(true);
  }

  public delete(post: User): Observable<boolean> {
    return Observable.of(true);
  }

  public report(post: User): Observable<boolean> {
    return Observable.of(true);
  }
}
