import { Component, Input  } from '@angular/core';

@Component({
  selector: 'preloader',
  template: require('./preloader.view.html')
})
export class PreloaderComponent {
  @Input()
  show: boolean;

  @Input()
  primary: boolean;

}
