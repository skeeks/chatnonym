import { Component, Input  } from '@angular/core';

@Component({
  selector: 'button-preloader',
  template: require('./button.preloader.view.html')
})
export class ButtonPreloaderComponent {
  @Input()
  show: boolean;

  @Input()
  primary: boolean;

}
