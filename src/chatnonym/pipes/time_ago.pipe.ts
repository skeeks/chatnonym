import { Pipe, PipeTransform } from '@angular/core';

import * as moment from 'moment';
import 'moment/locale/de';
moment.locale('de');
/*
 * Formats a timestamp into a string of the form 'from X minutes ago'
*/
@Pipe({name: 'time_ago'})
export class TimeAgoPipe implements PipeTransform {
  transform(value: number): string {
    return moment(new Date(value)).fromNow();
  }
}
