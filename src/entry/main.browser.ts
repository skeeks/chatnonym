/*
 * Providers provided by Angular
 */
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';


// The app module
import { ChatnonymModule } from 'chatnonym/app/chatnonym.module';



export function main() {
  platformBrowserDynamic().bootstrapModule(ChatnonymModule);
  eval('FastClick.attach(document.body);');
}

// bootstrap when document is ready
document.addEventListener('DOMContentLoaded', () => main());
