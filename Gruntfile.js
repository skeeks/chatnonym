module.exports = function(grunt) {

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.initConfig({
    sass: { // Task
      build: { // Target
        options: { // Target options
          style: 'expanded',
          sourcemap: 'auto'
        },
        files: { // Dictionary of files
          'www/css/chatnonym.css': 'sass/chatnonym.scss'
        }
      }
    },
    watch: {
      files: ['sass/**.scss'],
      tasks: ['sass']
    }
  });

  grunt.registerTask('default', ['sass', 'watch']);
};
